# Op Multihash

`op_multihash` is an Anagolay Operation that generates blake3-256 or sha2-256 multihash as configured.
It requires the `Bytes` to hash as input and provides an `op_multihash::U64MultihashWrapper` output, 
according to its manifest data:
```
{
 "name": "op_multihash",
  "desc": "Anagolay operation to generate multihash.",
  "input": [
      "Bytes",
  ],
  "config": {
    "hasher": ["sha2_256", "blake3_256"]
  },
  "groups": [
    "SYS"
  ],
  "output": "op_multihash::U64MultihashWrapper",
  "repository": "https://gitlab.com/anagolay/operations/op_multihash",
  "license": "Apache 2.0",
  "features": [
    "config_hasher",
    "std"
  ],
}
```

`op_multihash::U64MultihashWrapper` is a serializable wrapper for a multihash that uses unsigned integer of 64 bits.

## Installation

In the Cargo.toml file of your Workflow, specify a dependency toward **op_multihash** crate:

```toml
[dependencies]
op_multihash = { git = "https://gitlab.com/anagolay/operations/op_multihash" }
```

This crate uses IPFS dependencies. In order for the git fetch to work, `cargo` must fetch via system git instead of the 
local, with the option [netgit-fetch-with-cli](https://doc.rust-lang.org/cargo/reference/config.html#netgit-fetch-with-cli)
This configuration is stored in `.cargo/config.toml`

## Features

* **anagolay** Used to build this crate as dependency for other operations (default)
* **js** Used to build the javascript bindings (default)
* **std** Enable preferential compilation for std environment (default)
* **debug_assertions** Used to produce helpful debug messages (default), requires **std**
* **test** Used to build and execute tests, requires **std**

The following configuration features are available. They are intended to be available upon selection of the respective
value in Operation configuration

* **config_hasher_sha2_256**
* **config_hasher_blake3_256**

## Usage

### Execution

From Rust:

```rust
use op_multihash;

let input = b"example";
let config = std::collections::BTreeMap::from_iter([("hasher".to_string(), "blake3_256".to_string())]);
let output = op_multihash::execute(input, config).await.unwrap();
```
Or in WASM environment:

```javascript
import { execute } from "@anagolay/op_multihash"

async function main() {
  const input = "example";
  const config = { hasher: "blake3_256" }
  let output = await execute([input], config)
}
```

## Development

It is suggested to use the Vscode `.devcontainer` or any other sandboxed environment like Gitpod because the Operations must be developed under the same environment as they are published.

Git hooks will be initialized on first build or a test. If you wish to init them before run this in your terminal:

```shell
rusty-hook init
```

To help you with writing consistent code we provide you with following:

-   `.gitlab-ci.yml` for your testing and fmt checks
-   `rusty-hook.toml` for the `pre-push` fmt check so your CI is green.

### Manifest generation

```shell
makers manifest
```

## License

[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
